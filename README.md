# Project Structure #

ASP.NET Core WebAPI + Console Client

### Notes ###

* ASP.NET Core WebAPI Application
* Console Application
* Dependency Injection
* DTO 
* Project Structure
* Repository, UnitOfWork
* REST API
* CRUD
