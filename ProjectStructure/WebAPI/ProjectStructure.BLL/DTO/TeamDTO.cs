﻿using System;
using System.Collections.Generic;

namespace ProjectStructure.BLL.DTO
{
    public sealed class TeamDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}
