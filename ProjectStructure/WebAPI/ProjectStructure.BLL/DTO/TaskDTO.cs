﻿using System;
using ProjectStructure.DAL.Enums;

namespace ProjectStructure.BLL.DTO
{
    public sealed class TaskDTO
    {
        public int Id { get; set; }

        public int ProjectId { get; set; }

        public int PerformerId { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public TaskState State { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}
