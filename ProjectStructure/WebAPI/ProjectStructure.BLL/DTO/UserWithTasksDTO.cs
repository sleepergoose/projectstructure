﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.DTO
{
    public sealed class UserWithTasksDTO : UserDTO
    {
        public List<TaskDTO> Tasks { get; set; }
    }
}
