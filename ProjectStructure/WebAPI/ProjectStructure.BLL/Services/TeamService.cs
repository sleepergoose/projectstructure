﻿using AutoMapper;
using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.BLL.Services.Abstract;

namespace ProjectStructure.BLL.Services
{
    public sealed class TeamService : BaseService
    {
        public TeamService(IUnitOfWork unitOfWork, IMapper mapper)
            : base(unitOfWork, mapper)
        { }


        public IEnumerable<TeamDTO> GetTeams()
        {
            var teams = _unitOfWork.Teams.GetAll();

            return _mapper.Map<IEnumerable<TeamDTO>>(teams);
        }


        public TeamDTO GetTeam(int id)
        {
            var team = _unitOfWork.Teams.Get(id);

            return _mapper.Map<TeamDTO>(team);
        }


        public void AddTeam(TeamDTO teamDTO)
        {
            var team = _mapper.Map<Team>(teamDTO);

            _unitOfWork.Teams.Create(team);
        }


        public void UpdateTeam(TeamDTO teamDTO)
        {
            var team = _mapper.Map<Team>(teamDTO);

            _unitOfWork.Teams.Update(team);
        }


        public void DeleteTeam(int id)
        {
            _unitOfWork.Teams.Delete(id);
        }
    }
}
