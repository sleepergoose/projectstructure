﻿using System;
using AutoMapper;
using System.Linq;
using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.BLL.Services.Abstract;

namespace ProjectStructure.BLL.Services
{
    public sealed class LinqService : BaseService
    {
        private readonly List<Project> _projects;
        private readonly List<Team> _teams;
        private readonly List<User> _users;
        private readonly List<Task> _tasks;

        private List<Project> Projects { get; }

        public LinqService(IUnitOfWork unitOfWork, IMapper mapper)
            : base(unitOfWork, mapper)
        {
            _projects = _unitOfWork.Projects.GetAll().ToList();
            _teams = _unitOfWork.Teams.GetAll().ToList();
            _users = _unitOfWork.Users.GetAll().ToList();
            _tasks = _unitOfWork.Tasks.GetAll().ToList();


            Projects = _projects.GroupJoin(
                _tasks.Join(_users, task => task.PerformerId, user => user.Id, (task, user) =>
                {
                    task.Performer = user;
                    return task;
                }),
                project => project.Id, task => task.ProjectId, (project, _tasks) =>
                {
                    project.Tasks = _tasks as ICollection<ProjectStructure.DAL.Entities.Task>;
                    return project;
                })
                .Join(_users, project => project.AuthorId, user => user.Id, (project, user) =>
                {
                    project.Author = user;
                    return project;
                })
                .Join(_teams, project => project.TeamId, team => team.Id, (project, team) =>
                {
                    project.Team = team;
                    return project;
                }).ToList();
        }


        /* -- 1 -- */
        public Dictionary<ProjectDTO, int> GetTasksAmount(int authorId)
        {
            var result = Projects.Where(project => project.AuthorId == authorId)
                .ToDictionary(key => key, value => (value.Tasks == null ? 0 : value.Tasks.Count()));
            return _mapper.Map<Dictionary<ProjectDTO, int>>(result);
        }



        /* -- 2 -- */
        public List<TaskDTO> GetTasksList(int performerId)
        {
            var result = Projects.Where(project => project.Tasks != null)
                .SelectMany(project => project.Tasks)
                .Where(task => task.PerformerId == performerId && task.Name.Length < 45)
                .ToList();

            return _mapper.Map<List<TaskDTO>>(result);
        }



        /* -- 3 -- */
        public List<(int Id, string Name)> GetFinishedTasks(int performerId)
        {
            return Projects.Where(project => project.Tasks != null)
                .SelectMany(project => project.Tasks)
                .Where(task =>
                        task.PerformerId == performerId &&
                        task.FinishedAt?.Year == DateTime.Now.Year)
                .Select(task => (id: task.Id, name: task.Name))
                .ToList();
        }


        /* -- 4 -- */
        public List<(int Id, string TeamName, List<UserDTO> Users)> GetTeamsMembers()
        {
            return Projects.Select(p => p.Team).Where(team =>
                {
                    return !Projects.Where(p => p.Tasks != null)
                         .SelectMany(p => p.Tasks.Select(task => task.Performer))
                         .Where(perfomer => perfomer.BirthDay.Year > DateTime.Now.Year - 10)
                         .Any(perfomer => perfomer.TeamId == team.Id);
                })
                   .Distinct()
                   .Select(team =>
                       (
                           Id: team.Id,
                           TeamName: team.Name,
                           Users: _mapper.Map<List<UserDTO>>(Projects.Where(p => p.Tasks != null)
                                       .SelectMany(p => p.Tasks.Select(task => task.Performer))
                                       .Distinct()
                                       .Where(performer => performer.TeamId == team.Id)
                                       .OrderByDescending(performer => performer.RegisteredAt).ToList())
                       )).ToList();
        }



        /* -- 5 -- */
        public List<UserWithTasksDTO> GetUsersWithTasks()
        {
            return Projects.Where(project => project.Tasks != null)
                .SelectMany(project => project.Tasks.Select(task => task.Performer))
                .Distinct()
                .OrderBy(performer => performer.FirstName)
                .Select(performer => new UserWithTasksDTO
                {
                    Id = performer.Id,
                    FirstName = performer.FirstName,
                    BirthDay = performer.BirthDay,
                    Email = performer.Email,
                    LastName = performer.LastName,
                    RegisteredAt = performer.RegisteredAt,
                    TeamId = performer.TeamId,
                    Tasks = _mapper.Map<List<TaskDTO>>(Projects.Where(project => project.Tasks != null)
                                                            .SelectMany(p => p.Tasks)
                                                            .Where(task => task.PerformerId == performer.Id)
                                                            .OrderByDescending(user => user.Name.Length)
                                                            .ToList())
                }).ToList();
        }


        /* -- 6 -- */
        public UserSummaryDTO GetUserSummary(int userId)
        {
            return Projects.Where(project => project.Tasks != null)
                .SelectMany(project => project.Tasks.Select(task => task.Performer))
                .Distinct()
                .Where(user => user.Id == userId)
                .Select(user =>
                {

                    var lastUserProject = _projects
                        .Where(p => p.AuthorId == userId)
                        .OrderByDescending(p => p.CreatedAt)
                        .FirstOrDefault();

                    return new UserSummaryDTO
                    {
                        User = _mapper.Map<UserDTO>(user),
                        LastProject = _mapper.Map<ProjectDTO>(lastUserProject),
                        LastProjectTasksAmount = lastUserProject?.Tasks == null ? 0 : lastUserProject.Tasks.Count(),

                        BadTasksAmount = Projects.Where(project => project.Tasks != null)
                                            .SelectMany(project => project.Tasks)
                                            .Where(task => task.PerformerId == user.Id && task.FinishedAt == null)
                                            .Count(),

                        LongestTask = _mapper.Map<TaskDTO>(Projects.Where(project => project.Tasks != null)
                                            .SelectMany(project => project.Tasks)
                                            .Where(task => task.PerformerId == user.Id)
                                            .OrderByDescending(t => (t.FinishedAt == null ? DateTime.Now : t.FinishedAt) - t.CreatedAt)
                                            .FirstOrDefault())
                    };
                }).FirstOrDefault();
        }


        /* -- 7 -- */
        public List<ProjectSummaryDTO> GetProjectSummary()
        {
            return Projects.Select(project =>
                 new ProjectSummaryDTO
                 {
                     Project = _mapper.Map<ProjectDTO>(project),
                     LongestTaskByDescription = _mapper.Map<TaskDTO>(project.Tasks?.OrderByDescending(task => task.Description.Length).FirstOrDefault()),
                     ShortestTaskByName = _mapper.Map<TaskDTO>(project.Tasks?.OrderBy(task => task.Name.Length).FirstOrDefault()),
                     TeamMemberAmount = (project.Description.Length > 20 || (project.Tasks == null ? 0 : project.Tasks.Count()) < 3)
                                        ? (project.Team == null ? 0 :
                                            _projects.Where(p => p.Tasks != null)
                                                .SelectMany(p => p.Tasks.Select(t => t.Performer))
                                                .Distinct()
                                                .Where(u => u.TeamId == project.TeamId)
                                                .Count()) : 0
                 }).ToList();
        }

    }
}
