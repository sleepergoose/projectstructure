﻿using AutoMapper;
using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.BLL.Services.Abstract;

namespace ProjectStructure.BLL.Services
{
    public sealed class UserService : BaseService
    {
        public UserService(IUnitOfWork unitOfWork, IMapper mapper)
            : base(unitOfWork, mapper)
        { }


        public IEnumerable<UserDTO> GetUsers()
        {
            var users = _unitOfWork.Users.GetAll();

            return _mapper.Map<IEnumerable<UserDTO>>(users);
        }

        public UserDTO GetUser(int id)
        {
            var user = _unitOfWork.Users.Get(id);

            return _mapper.Map<UserDTO>(user);
        }

        public void AddUser(UserDTO userDTO)
        {
            var user = _mapper.Map<User>(userDTO);

            _unitOfWork.Users.Create(user);
        }


        public void UpdateUser(UserDTO userDTO)
        {
            var user = _mapper.Map<User>(userDTO);

            _unitOfWork.Users.Update(user);
        }


        public void DeleteUser(int id)
        {
            _unitOfWork.Users.Delete(id);
        }
    }
}
