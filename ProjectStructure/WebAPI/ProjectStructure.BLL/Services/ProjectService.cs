﻿using AutoMapper;
using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.BLL.Services.Abstract;

namespace ProjectStructure.BLL.Services
{
    public sealed class ProjectService : BaseService
    {

        public ProjectService(IUnitOfWork unitOfWork, IMapper mapper)
            : base(unitOfWork, mapper)
        { }


        public IEnumerable<ProjectDTO> GetProjects()
        {
            var projects = _unitOfWork.Projects.GetAll();

            return _mapper.Map<IEnumerable<ProjectDTO>>(projects);
        }


        public ProjectDTO GetProject(int id)
        {
            var project = _unitOfWork.Projects.Get(id);

            return _mapper.Map<ProjectDTO>(project);
        }


        public void AddProject(ProjectDTO projectDTO)
        {
            var project = _mapper.Map<Project>(projectDTO);

            _unitOfWork.Projects.Create(project);
        }


        public void UpdateProject(ProjectDTO projectDTO)
        {
            var project = _mapper.Map<Project>(projectDTO);

            _unitOfWork.Projects.Update(project);
        }


        public void DeleteProject(int id)
        {
            _unitOfWork.Projects.Delete(id);
        }
    }
}
