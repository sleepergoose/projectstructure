﻿using AutoMapper;
using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.BLL.Services.Abstract;

namespace ProjectStructure.BLL.Services
{
    public sealed class TaskService : BaseService
    {
        public TaskService(IUnitOfWork unitOfWork, IMapper mapper)
            : base(unitOfWork, mapper)
        { }


        public IEnumerable<TaskDTO> GetTasks()
        {
            var tasks = _unitOfWork.Tasks.GetAll();

            return _mapper.Map<IEnumerable<TaskDTO>>(tasks);
        }


        public TaskDTO GetTask(int id)
        {
            var task = _unitOfWork.Tasks.Get(id);

            return _mapper.Map<TaskDTO>(task);
        }


        public void AddTask(TaskDTO taskDTO)
        {
            var task = _mapper.Map<ProjectStructure.DAL.Entities.Task>(taskDTO);

            _unitOfWork.Tasks.Create(task);
        }


        public void UpdateTask(TaskDTO taskDTO)
        {
            var task = _mapper.Map<ProjectStructure.DAL.Entities.Task>(taskDTO);

            _unitOfWork.Tasks.Update(task);
        }


        public void DeleteTask(int id)
        {
            _unitOfWork.Tasks.Delete(id);
        }
    }
}
