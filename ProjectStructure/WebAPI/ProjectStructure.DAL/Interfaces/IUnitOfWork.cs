﻿using ProjectStructure.DAL.Entities;

namespace ProjectStructure.DAL.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<Project> Projects { get; }

        IRepository<User> Users { get; }

        IRepository<Team> Teams { get; }

        IRepository<ProjectStructure.DAL.Entities.Task> Tasks { get; }
    }
}
