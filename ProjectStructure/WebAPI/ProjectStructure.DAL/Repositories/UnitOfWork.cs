﻿using System;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.DAL.Repositories;

namespace ProjectStructure.DAL.Repositories
{
    public sealed class UnitOfWork : IUnitOfWork
    {
        private readonly Lazy<ProjectRepository> _projectRepository;

        private readonly Lazy<UserRepository> _userRepository;

        private readonly Lazy<TeamRepository> _teamRepository;

        private readonly Lazy<TaskRepository> _taskRepository;


        public UnitOfWork()
        {
            _projectRepository = new Lazy<ProjectRepository>();
            _userRepository = new Lazy<UserRepository>();
            _teamRepository = new Lazy<TeamRepository>();
            _taskRepository = new Lazy<TaskRepository>();
        }


        /* Properties */
        public IRepository<Project> Projects => _projectRepository.Value;

        public IRepository<User> Users => _userRepository.Value;

        public IRepository<Team> Teams => _teamRepository.Value;

        public IRepository<Task> Tasks => _taskRepository.Value;
    }
}
