﻿using System;
using System.Linq;
using System.Collections.Generic;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;



namespace ProjectStructure.DAL.Repositories
{
    public sealed class TeamRepository : IRepository<Team>
    {
        private List<Team> _teams;



        public Team Get(int id)
        {
            return _teams?.FirstOrDefault(p => p.Id == id);
        }


        public IEnumerable<Team> GetAll()
        {
            return _teams;
        }


        public void Create(Team item)
        {
            if (item != null)
            {
                int maxId = _teams.Select(t => t.Id).Max();
                item.Id = maxId + 1;
                _teams.Add(item);
            }
        }


        public void Delete(int id)
        {
            var temp = _teams.FirstOrDefault(p => p.Id == id);

            if (temp != null)
                _teams.Remove(temp);
        }


        public void Update(Team item)
        {
            var temp = _teams.FirstOrDefault(p => p.Id == item.Id);

            if (temp != null)
            {
                _teams[_teams.IndexOf(temp)] = item;
            }
        }



        public TeamRepository()
        {
            _teams = new List<Team>()
            {
                new Team() {
                    Id = 0,
                    Name = "Denesik - Greenfelder",
                    CreatedAt = DateTime.Parse( "2019-08-25T15:48:06.062331+00:00")
                },
                new Team() {
                    Id = 1,
                    Name = "Durgan Group",
                    CreatedAt = DateTime.Parse( "2017-03-31T02:29:28.3740504+00:00")
                },
                new Team() {
                    Id = 2,
                    Name = "Kassulke LLC",
                    CreatedAt = DateTime.Parse( "2019-02-21T15:47:30.3797852+00:00")
                },
                new Team() {
                    Id = 3,
                    Name = "Harris LLC",
                    CreatedAt = DateTime.Parse( "2018-08-28T08:18:46.4160342+00:00")
                },
                new Team() {
                    Id = 4,
                    Name = "Mitchell Inc",
                    CreatedAt = DateTime.Parse( "2019-04-03T09:58:33.0178179+00:00")
                },
                new Team() {
                    Id = 5,
                    Name = "Smitham Group",
                    CreatedAt = DateTime.Parse( "2016-10-05T07:57:02.8427653+00:00")
                },
                new Team() {
                    Id = 6,
                    Name = "Kutch - Roberts",
                    CreatedAt = DateTime.Parse( "2016-10-31T05:05:15.1076578+00:00")
                },
                new Team() {
                    Id = 7,
                    Name = "Parisian Group",
                    CreatedAt = DateTime.Parse( "2016-07-17T01:34:55.0917082+00:00")
                },
                new Team() {
                    Id = 8,
                    Name = "Schiller Group",
                    CreatedAt = DateTime.Parse( "2020-10-05T01:20:14.1953926+00:00")
                },
                new Team() {
                    Id = 9,
                    Name = "Littel, Turcotte and Muller",
                    CreatedAt = DateTime.Parse( "2018-10-19T14:54:27.5549549+00:00")
                }
            };
        }
    }
}
