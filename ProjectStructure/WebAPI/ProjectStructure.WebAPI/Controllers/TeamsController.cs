﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using ProjectStructure.BLL.Services;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly TeamService _teamService;


        public TeamsController(TeamService teamService)
        {
            _teamService = teamService;
        }


        [HttpGet]
        public ActionResult<IEnumerable<TeamDTO>> Get()
        {
            return Ok(_teamService.GetTeams());
        }


        [HttpGet("{id}")]
        public ActionResult<TeamDTO> Get(int id)
        {
            return Ok(_teamService.GetTeam(id));
        }


        [HttpPost]
        public ActionResult Post([FromBody] TeamDTO teamDTO)
        {
            _teamService.AddTeam(teamDTO);

            return NoContent();
        }


        [HttpPut]
        public ActionResult Put([FromBody] TeamDTO teamDTO)
        {
            _teamService.UpdateTeam(teamDTO);

            return NoContent();
        }


        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _teamService.DeleteTeam(id);

            return NoContent();
        }
    }
}
