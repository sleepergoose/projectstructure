﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using ProjectStructure.BLL.Services;


namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly ProjectService _projectService;


        public ProjectsController(ProjectService projectService)
        {
            _projectService = projectService;
        }


        [HttpGet]
        public ActionResult<IEnumerable<ProjectDTO>> Get()
        {
            return Ok(_projectService.GetProjects());
        }


        [HttpGet("{id}")]
        public ActionResult<ProjectDTO> Get(int id)
        {
            return Ok(_projectService.GetProject(id));
        }


        [HttpPost]
        public ActionResult Post([FromBody] ProjectDTO projectDTO)
        {
            _projectService.AddProject(projectDTO);

            return NoContent();
        }


        [HttpPut]
        public ActionResult Put([FromBody] ProjectDTO projectDTO)
        {
            _projectService.UpdateProject(projectDTO);

            return NoContent();
        }


        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _projectService.DeleteProject(id);

            return NoContent();
        }
    }
}
