﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using ProjectStructure.BLL.Services;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {

        private readonly TaskService _taskService;


        public TasksController(TaskService taskService)
        {
            _taskService = taskService;
        }


        [HttpGet]
        public ActionResult<IEnumerable<TaskDTO>> Get()
        {
            return Ok(_taskService.GetTasks());
        }


        [HttpGet("{id}")]
        public ActionResult<TaskDTO> Get(int id)
        {
            return Ok(_taskService.GetTask(id));
        }


        [HttpPost]
        public ActionResult Post([FromBody] TaskDTO taskDTO)
        {
            _taskService.AddTask(taskDTO);

            return NoContent();
        }


        [HttpPut]
        public ActionResult Put([FromBody] TaskDTO taskDTO)
        {
            _taskService.UpdateTask(taskDTO);

            return NoContent();
        }


        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _taskService.DeleteTask(id);

            return NoContent();
        }
    }
}
