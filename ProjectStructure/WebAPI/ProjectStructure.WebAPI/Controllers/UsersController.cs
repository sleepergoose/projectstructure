﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using ProjectStructure.BLL.Services;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UserService _userService;


        public UsersController(UserService userService)
        {
            _userService = userService;
        }


        [HttpGet]
        public ActionResult<IEnumerable<UserDTO>> Get()
        {
            return Ok(_userService.GetUsers());
        }


        [HttpGet("{id}")]
        public ActionResult<UserDTO> Get(int id)
        {
            return Ok(_userService.GetUser(id));
        }


        [HttpPost]
        public ActionResult Post([FromBody] UserDTO userDTO)
        {
            _userService.AddUser(userDTO);

            return NoContent();
        }


        [HttpPut]
        public ActionResult Put([FromBody] UserDTO userDTO)
        {
            _userService.UpdateUser(userDTO);

            return NoContent();
        }


        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _userService.DeleteUser(id);

            return NoContent();
        }
    }
}
