﻿using System.Collections.Generic;
using ProjectStructure.Common.Entities;

namespace ProjectStructure.Client.Interfaces
{
    public interface IQueries
    {
        List<(int Id, string Name)> GetFinishedTasks(int performerId);

        List<ProjectSummary> GetProjectSummary();

        Dictionary<Project, int> GetTasksAmount(int authorId);

        List<ProjectStructure.Common.Entities.Task> GetTasksList(int performerId);

        List<(int Id, string TeamName, List<User> Users)> GetTeamsMembers();

        UserSummary GetUserSummary(int userId);

        List<User> GetUsersWithTasks();
    }
}
