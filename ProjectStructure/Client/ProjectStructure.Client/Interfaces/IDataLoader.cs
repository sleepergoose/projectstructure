﻿using System.Threading.Tasks;
using System.Collections.Generic;
using ProjectStructure.Common.Entities;

namespace ProjectStructure.Client.Interfaces
{
    public interface IDataLoader
    {
        Task<List<Project>> GetDataStructure();
    }
}
