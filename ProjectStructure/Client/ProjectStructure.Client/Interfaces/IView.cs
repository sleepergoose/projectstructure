﻿using ProjectStructure.Common.DTO;
using ProjectStructure.Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.Client.Interfaces
{
    public interface IView
    {
        void ShowMenu();

        void WriteTextToCenter(string text);

        void ShowTaskAmount(Dictionary<Project, int> dict);

        void ShowTaskList(List<ProjectStructure.Common.Entities.Task> tasks);

        void ShowFinishedTasks(List<(int Id, string Name)> list);

        void ShowTeamsMembers(List<(int Id, string TeamName, List<User> Users)> teams);

        void ShowUsersWithTasks(List<User> users);

        void ShowUserSummary(UserSummary userSummary);

        void ShowProjectSummary(List<ProjectSummary> projectSummaries);

        void WriteErrorToCenter(string error);

        string GetTextResponse(string message);



    }
}
