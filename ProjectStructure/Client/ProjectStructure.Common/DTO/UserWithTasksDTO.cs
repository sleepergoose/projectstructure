﻿using System;
using System.Collections.Generic;


namespace ProjectStructure.Common.DTO
{
    public sealed class UserWithTasksDTO : UserDTO
    {
        public List<TaskDTO> Tasks { get; set; }
    }
}
