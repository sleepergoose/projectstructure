﻿using System;
using Lecture1.Common.Enums;

namespace ProjectStructure.Common.Entities
{
    public sealed class Task
    {
        public int Id { get; set; }

        public int ProjectId { get; set; }
        public Project Project { get; set; }

        public int PerformerId { get; set; }
        public User Performer { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public TaskState State { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}
