﻿using System;
using System.Collections.Generic;

namespace ProjectStructure.Common.Entities
{
    public sealed class User
    {
        public User()
        {
            Tasks = new List<Task>();
        }

        public int Id { get; set; }

        public int? TeamId { get; set; }
        public Team Team { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }

        public ICollection<Task> Tasks { get; set; }
    }
}
