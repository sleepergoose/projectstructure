﻿using ProjectStructure.Common.DTO;
using System.Threading.Tasks;
using ProjectStructure.BLL.Services.Abstract;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace ProjectStructure.BLL.Services
{
    public class TaskService : BaseService
    {
        private string _url;

        public TaskService(string host) : base(host)
        {
            _url = $"{Host}/api/Tasks";
        }

        public async Task<IEnumerable<ProjectStructure.Common.Entities.Task>> GetAllTasks()
        {
            var response = await _httpService.GetAsync(_url);

            var tasks = JsonConvert.DeserializeObject<ICollection<TaskDTO>>(response);

            return _mapper.Map<IEnumerable<ProjectStructure.Common.Entities.Task>>(tasks);
        }


        public async Task<ProjectStructure.Common.Entities.Task> GetTask(int id)
        {
            var response = await _httpService.GetAsync($"{_url}/{id}");

            var task = JsonConvert.DeserializeObject<TaskDTO>(response);

            return _mapper.Map<ProjectStructure.Common.Entities.Task>(task);
        }
    }
}
