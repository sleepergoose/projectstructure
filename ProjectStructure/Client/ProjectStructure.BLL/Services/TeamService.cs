﻿using ProjectStructure.Common.DTO;
using System.Threading.Tasks;
using ProjectStructure.BLL.Services.Abstract;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using ProjectStructure.Common.Entities;

namespace ProjectStructure.BLL.Services
{
    public class TeamService : BaseService
    {
        private string _url;

        public TeamService(string host) : base(host)
        {
            _url = $"{Host}/api/Teams";
        }

        public async Task<IEnumerable<Team>> GetAllTeams()
        {
            var response = await _httpService.GetAsync(_url);

            var teams = JsonConvert.DeserializeObject<ICollection<TeamDTO>>(response);

            return _mapper.Map<IEnumerable<Team>>(teams);
        }


        public async Task<Team> GetTeam(int id)
        {
            var response = await _httpService.GetAsync($"{_url}/{id}");
            
            var team = JsonConvert.DeserializeObject<TeamDTO>(response);
            
            return _mapper.Map<Team>(team);
        }
    }
}
