﻿using ProjectStructure.Common.DTO;
using System.Threading.Tasks;
using ProjectStructure.BLL.Services.Abstract;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using System;
using System.Text;
using ProjectStructure.Common.Entities;

namespace ProjectStructure.BLL.Services
{
    public class ProjectService : BaseService
    {
        private string _url;

        public ProjectService(string host) : base(host)
        {
            _url = $"{Host}/api/Projects"; 
        }


        public async Task<IEnumerable<Project>> GetAllProgects()
        {
            var response = await _httpService.GetAsync(_url);

            var projects = JsonConvert.DeserializeObject<ICollection<ProjectDTO>>(response);

            return _mapper.Map<IEnumerable<Project>>(projects);
        }


        public async Task<Project> GetProgect(int id)
        {
            var response = await _httpService.GetAsync($"{_url}/{id}");

            var project = JsonConvert.DeserializeObject<ProjectDTO>(response);

            return _mapper.Map<Project>(project);
        }
    }
}
