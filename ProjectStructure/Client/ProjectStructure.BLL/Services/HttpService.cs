﻿using System.Net.Http;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public sealed class HttpService
    {
        private readonly HttpClient _httpClient = new HttpClient();

        public async Task<string> GetAsync(string url)
        {
            return await _httpClient.GetStringAsync(url);
        }

    }
}
