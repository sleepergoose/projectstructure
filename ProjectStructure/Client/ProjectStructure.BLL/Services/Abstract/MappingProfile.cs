﻿using AutoMapper;
using ProjectStructure.Common.DTO;
using ProjectStructure.Common.Entities;

namespace ProjectStructure.BLL.Services.Abstract
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Team, TeamDTO>();
            CreateMap<TeamDTO, Team>();

            CreateMap<User, UserDTO>();
            CreateMap<UserDTO, User>();

            CreateMap<Project, ProjectDTO>();
            CreateMap<ProjectDTO, Project>();

            CreateMap<ProjectStructure.Common.Entities.Task, TaskDTO>();
            CreateMap<TaskDTO, ProjectStructure.Common.Entities.Task>();

            CreateMap<UserSummary, UserSummaryDTO>();
            CreateMap<UserSummaryDTO, UserSummary>();


            CreateMap<ProjectSummary, ProjectSummaryDTO>();
            CreateMap<ProjectSummaryDTO, ProjectSummary>();
        }
    }
}
