﻿using Newtonsoft.Json;
using ProjectStructure.BLL.Services.Abstract;
using ProjectStructure.Common.DTO;
using ProjectStructure.Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public class LinqService : BaseService
    {
        private string _url;

        public LinqService(string host) : base(host)
        {
            _url = $"{Host}/api/Linq";
        }

        /* -- 1 -- */
        public async Task<Dictionary<Project, int>> GetTasksAmount(int authorId)
        {
            var response = await _httpService.GetAsync($"{_url}/TasksAmount/{authorId}");

            var list = JsonConvert.DeserializeObject<List<KeyValuePair<ProjectDTO, int>>>(response);

            var dict = list.ToDictionary(x => x.Key, x => x.Value);

            return _mapper.Map<Dictionary<Project, int>>(dict); ;
        }


        /* -- 2 -- */
        public async Task<List<ProjectStructure.Common.Entities.Task>> GetTasksList(int performerId)
        {
            var response = await _httpService.GetAsync($"{_url}/TasksList/{performerId}");

            var tasks = JsonConvert.DeserializeObject<List<TaskDTO>>(response);

            return _mapper.Map<List<ProjectStructure.Common.Entities.Task>>(tasks); ;
        }


        /* -- 3 -- */
        public async Task<List<(int Id, string Name)>> GetFinishedTasks(int performerId)
        {
            var response = await _httpService.GetAsync($"{_url}/FinishedTasks/{performerId}");

            var tasks = JsonConvert.DeserializeObject<List<(int Id, string Name)>>(response);

            return tasks;
        }

        /* -- 4 -- */
        public async Task<List<(int Id, string TeamName, List<User> Users)>> GetTeamsMembers()
        {
            var response = await _httpService.GetAsync($"{_url}/TeamsMembers");

            var tasks = JsonConvert.DeserializeObject<List<(int Id, string TeamName, List<UserDTO> Users)>>(response);

            return tasks.Select(task => 
                    (Id: task.Id, TeamName: task.TeamName, Users: _mapper.Map<List<User>>(task.Users)))
                .ToList();
        }


        /* -- 5 -- */
        public async Task<List<User>> GetUsersWithTasks()
        {
            var response = await _httpService.GetAsync($"{_url}/UsersWithTasks");

            var users = JsonConvert.DeserializeObject<List<UserWithTasksDTO>>(response);

            return _mapper.Map<List<User>>(users)
                    .GroupJoin(users.SelectMany(u => u.Tasks), user => user.Id, task => task.PerformerId, 
                        (user, _tasks) => {
                            user.Tasks = _mapper.Map<List<Common.Entities.Task>>(_tasks);
                            return user;
                        })
                    .ToList();
        }


        /* -- 6 -- */
        public async Task<UserSummary> GetUserSummary(int userId)
        {
            var response = await _httpService.GetAsync($"{_url}/UserSummary/{userId}");

            var user = JsonConvert.DeserializeObject<UserSummaryDTO>(response);

            return _mapper.Map<UserSummary>(user);
        }


        /* -- 7 -- */
        public async Task<List<ProjectSummary>> GetProjectSummary()
        {
            var response = await _httpService.GetAsync($"{_url}/ProjectSummary");

            var projects = JsonConvert.DeserializeObject<List<ProjectSummaryDTO>>(response);

            return _mapper.Map<List<ProjectSummary>>(projects);
        }
    }
}
